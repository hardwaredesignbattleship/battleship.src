module pb_single_pulse (clk, PB, PB_single_pulse);
input clk;
input PB;
output PB_single_pulse;
wire PB_debounced;
debounce DB(PB_debounced, PB, clk);

reg PB_single_pulse;
reg PB_debounced_delay;
always @(posedge clk) begin
if (PB_debounced == 1'b1 & PB_debounced_delay == 1'b0)
PB_single_pulse <= 1'b1;
else
PB_single_pulse <= 1'b0;
PB_debounced_delay <= PB_debounced;
end
endmodule

