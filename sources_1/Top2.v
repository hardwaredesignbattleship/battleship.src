module Top2(
	input clk,
	input uart_rx,
	input button,
	input weapon_type,
	input ship_rotate,
	inout wire PS2_DATA,
	inout wire PS2_CLK,
	//output [3:0] DIGIT,
	//output [6:0] DISPLAY,
	output [5:0] lights,
	output [3:0] vgaRed,
	output [3:0] vgaGreen,
	output [3:0] vgaBlue,
	output hsync,
	output vsync,
	output uart_tx,
	
	output [3:0] debug_dnshift,
	output [3:0] debug_rtshift
);
	
	wire [511:0] key_down;
	wire [8:0] key_last_change;
	wire key_valid;
	KeyboardDecoder key_de (
		.key_down(key_down),
		.last_change(key_last_change),
		.key_valid(key_valid),
		.PS2_DATA(PS2_DATA),
		.PS2_CLK(PS2_CLK),
		.rst(),
		.clk(clk)
	);
	
	wire button_single_pulse;
	OnePulse single_pulse(
		.clock(clk),
		.signal(button),
		.signal_single_pulse(button_single_pulse)
	);
	
	wire lose;
	wire [99:0] storage_read_ship;
	wire [99:0] storage_read_hit;
	wire [99:0] storage_read_destroy;
	wire [99:0] storage_read_cusor;
	    
	 wire [99:0] storage_read_op_hit;
	 wire [99:0] storage_read_op_destroy;
	    
	  wire [2:0] storage_write_select;
	  wire [99:0] storage_write_value;
	  wire storage_write_enable;
	    
	  wire [2:0] screen_select;
	  
	 wire tx_has_data;
	 wire [7:0] rx_data;
	 wire [7:0] tx_data;
	 wire tx_available;
	 wire rx_has_data;
	 
	 //wire uart_clk;
	 /*
	 uart_clock	#11520	uclk(
	 	.clk(clk),
	 	.out_clk(uart_clk)
	 );
	 */
	 
	 wire clk_25;
	 //clk_wiz_0 clockMod( .clk(clk), .clk_out25(clk_25) );
	 clk_div13 #(2) clockMod( .clk(clk), .clk_div(clk_25) );
	 
	FSM2	fsm(
		.clk(clk),
		.lose(lose),
		.button(button_single_pulse),
		.rx_has_data(rx_has_data),
		.rx_data(rx_data),
		.tx_has_data(tx_has_data),
		.tx_available(tx_avilable),
		.tx_data(tx_data),
		.key_down(key_down),
		.key_last_change(key_last_change),
		.key_valid(key_valid),
		.storage_read_ship(storage_read_ship),
		.storage_read_hit(storage_read_hit),
		.storage_read_destroy(storage_read_destroy),
		.storage_read_cusor(storage_read_cusor),
		.storage_read_op_hit(storage_read_op_hit),
		.storage_read_op_destroy(storage_read_op_destroy),
		.storage_write_select(storage_write_select),
		.storage_write_value(storage_write_value),
		.storage_write_enable(storage_write_enable),
		.screen_select(screen_select),
		.lights(lights),
		.weapon_type(weapon_type),
		.ship_rotate(ship_rotate),
		
		.debug_dnshift(debug_dnshift),
		.debug_rtshift(debug_rtshift)
	);

	UartModule		uartMod(
		.clk(clk),
		.reset(),
		.load_tx_data(tx_has_data),
		.tx_out_data(tx_data),
		.available(tx_available),
		.has_rx_data(rx_has_data),
		.rx_in_data(rx_data),
		.uart_tx(uart_tx),
		.uart_rx(uart_rx)
	);
	
	FieldMapStorage storage( .clk(clk),
	                         .reset(),
	                         .category(storage_write_select),
	                         .write_enable(storage_write_enable),
	                         .write_value(storage_write_value),
	                         
	                         .read_value_ship(storage_read_ship),
	                         .read_value_destroy(storage_read_destroy),
	                         .read_value_hit(storage_read_hit),
	                         .read_value_cusor(storage_read_cusor),
	                         
	                         .read_value_op_destroy(storage_read_op_destroy),
	                         .read_value_op_hit(storage_read_op_hit) );
	
	wire valid;
	wire [9:0] h_cnt; //640
	wire [9:0] v_cnt;  //480
	wire [11:0] pixel;
	                              
	assign {vgaRed, vgaGreen, vgaBlue} = (valid==1'b1) ? pixel : 12'h0;
	                              
	vga_controller vga_inst(
											  .reset(),
											  .pclk(clk_25),
											  .hsync(hsync),
											  .vsync(vsync),
											  .valid(valid),
											  .h_cnt(h_cnt),
											  .v_cnt(v_cnt)
											);
					  
	  Display displayMod( .clk_25(clk_25),
	  					  .reset(),
						  .h_cnt(h_cnt),
						  .v_cnt(v_cnt),
						  .pixel(pixel),
						  
						  .storage_ship(storage_read_ship),
						  .storage_destroy(storage_read_destroy),
						  .storage_cusor(storage_read_cusor),
						  .storage_hit(storage_read_hit),
						  .storage_enemy_destroy(storage_read_op_destroy),
						  .storage_enemy_hit(storage_read_op_hit),
						  .scene_select(screen_select)
						   );
	
endmodule