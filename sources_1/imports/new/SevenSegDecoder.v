`timescale 1ns / 1ps

module SevenSegDecoder(
    input clk,
    input [3:0] BCD0,
    input [3:0] BCD1,
    output [6:0] DISPLAY,
    output [3:0] DIGIT
    );
    
    reg digitCounter;
    
    reg [6:0] r_display;
    assign DISPLAY = r_display;
    
    reg [3:0] r_digit;
    assign DIGIT = r_digit;
    
    reg [3:0] bcd;
    
    always @(posedge clk) begin
        if(digitCounter == 1'b0) begin
            bcd <= BCD0;
            r_digit <= 4'b1101;
        end else begin
            bcd <= BCD1;
            r_digit <= 4'b1110;
        end
        
        case(bcd)
            4'b0000: r_display <= 7'b1000000;
            4'b0001: r_display <= 7'b1111001;
            4'b0010: r_display <= 7'b0100100;
            4'b0011: r_display <= 7'b0110000;
            4'b0100: r_display <= 7'b0011001;
            4'b0101: r_display <= 7'b0010010;
            4'b0110: r_display <= 7'b0000010;
            4'b0111: r_display <= 7'b1111000;
            4'b1000: r_display <= 7'b0000000;
            4'b1001: r_display <= 7'b0010000;
            default: r_display <= 7'b1000000;
        endcase
        
        digitCounter <= digitCounter + 1'b1;
    end
    
endmodule
