`timescale 1ns / 1ps

module CustomClock(
    input clk,
    output clk_out
    );
    parameter power = 1;
    
    wire [(power-1) : 0] next_num;
    reg [(power-1) : 0] num;
    
    always @(posedge clk) begin
        num <= next_num;
    end
    
    assign next_num = num + 1'b1;
    
    assign clk_out = num[power - 1];
    
endmodule
