`timescale 1ns / 1ps

module OnePulse(
    input clk,
    input sigInput,
    output sigOutput
    );
    
    reg r_output;
    assign sigOutput = r_output;
    
    reg prev_input = 0;
    
    always @(posedge clk) begin
        if(sigInput == 1 && prev_input == 0) r_output <= 1;
        else r_output <= 0;
        
        prev_input <= sigInput;
    end
    
endmodule
