`timescale 1ns / 1ps

`define STATE_TX_READY  1
`define STATE_TX_WAIT   2
`define STATE_TX_SEND   3

module UartModule(
    input clk,
    input reset,
    //Tx
    input load_tx_data,
    input [7:0] tx_out_data,
    output available,
    //Rx
    output has_rx_data,
    output reg[7:0] rx_in_data,
    output uart_tx,
    input uart_rx
    );
    
    wire uart_rx_clk, uart_tx_clk;
    uart_clock #(.factor(4)) uartRxClk( .clk(clk), .out_clk(uart_rx_clk) );
    uart_clock #(.factor(0)) uartTxClk( .clk(clk), .out_clk(uart_tx_clk) );
        
    //Tx
    reg ld_tx_data = 0;
    reg tx_enable = 1;
    wire tx_empty;
    assign available = tx_empty;
    //Rx
    wire[7:0] rx_data;
    reg uld_rx_data = 0;
    reg rx_enable = 1;
    wire rx_empty;
    assign has_rx_data = ~rx_empty;
    uart uartMod( .reset(reset), .rxclk(uart_rx_clk), .txclk(uart_tx_clk),
                  .uld_rx_data(uld_rx_data), .rx_data(rx_data), .rx_enable(rx_enable), .rx_in(uart_rx), .rx_empty(rx_empty), 
                  .tx_out(uart_tx), .ld_tx_data(ld_tx_data), .tx_enable(tx_enable), .tx_empty(tx_empty), .tx_data(tx_out_data));
    
    /*Rx*/
    always @(posedge clk) begin
        if(rx_empty == 0) begin
            rx_in_data <= rx_data;
            uld_rx_data <= 1;
        end 
        else begin
            rx_in_data <= rx_data;
            uld_rx_data <= 0;
        end
    end
    
    /*Tx*/
    reg[2:0] state = `STATE_TX_READY;
    always @(posedge clk) begin
        if(reset == 1) begin
            state <= `STATE_TX_READY;
        end
        else begin
            case(state)
                `STATE_TX_READY: begin
                    if(tx_empty == 0) begin
                        ld_tx_data <= 0;
                    end
                    else begin
                        ld_tx_data <= ld_tx_data; 
                    end
                    
                    if(load_tx_data == 1) begin
                        if(tx_empty == 1) state <= `STATE_TX_SEND;
                        else state <= `STATE_TX_WAIT;
                    end
                    else state <= state;
                end
                
                `STATE_TX_WAIT: begin
                    if(tx_empty == 1) state <= `STATE_TX_SEND;
                    else state <= state;
                end 
                
                `STATE_TX_SEND: begin
                    ld_tx_data <= 1;
                    state <= `STATE_TX_READY;
                end
                
                default: begin
                    ld_tx_data <= 0;
                    state <= `STATE_TX_READY;
                end
            endcase
        end
    end
    
endmodule
