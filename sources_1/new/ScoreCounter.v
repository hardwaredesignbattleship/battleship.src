`timescale 1ns / 1ps

module ScoreCounter(
    input clk,
    output reg loose,
    input [99:0] storage_read_ship,
    input [99:0] storage_read_destroy
    );
    
    reg[6:0] ship_index = 0;
    reg[6:0] destroy_index = 0;
    
    reg[6:0] ship_counter = 0;
    reg[6:0] destroy_counter = 0;
    always @(posedge clk) begin
        if(destroy_counter != 0 && destroy_counter >= ship_counter) begin
            loose <= 1;
        end
        else begin
            loose <= 0;
            
            //Ship
            if(ship_index < 100) begin
                ship_index <= ship_index + 1;
                
                if(storage_read_ship[ship_index] == 1) ship_counter <= ship_counter + 1;
                else ship_counter <= ship_counter;
            end
            else begin
                ship_index <= 0;
                ship_counter <= 0;
            end
            
            //Destroy
            if(destroy_index < 100) begin
                destroy_index <= destroy_index + 1;
                
                if(storage_read_destroy[destroy_index] == 1) destroy_counter <= destroy_counter + 1;
                else destroy_counter <= destroy_counter;
            end
            else begin
                destroy_index <= 0;
                destroy_counter <= 0;
            end
        end
    end
    
endmodule
