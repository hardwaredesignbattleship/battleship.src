`timescale 1ns / 1ps

module TestScreenTop(
    input clk,
    input rst,
    output [3:0] vgaRed,
    output [3:0] vgaGreen,
    output [3:0] vgaBlue,
    output hsync,
    output vsync
    );
    
    wire clk_25;
    clk_wiz_0 clockMod( .clk(clk), .clk_out25(clk_25) );
    
    wire valid;
    wire [9:0] h_cnt; //640
    wire [9:0] v_cnt;  //480
    wire [11:0] pixel;

    assign {vgaRed, vgaGreen, vgaBlue} = (valid==1'b1) ? pixel : 12'h0;
    
    vga_controller   vga_inst(
      .pclk(clk_25),
      .reset(rst),
      .hsync(hsync),
      .vsync(vsync),
      .valid(valid),
      .h_cnt(h_cnt),
      .v_cnt(v_cnt)
    );
    
    MapBlockDrawer blockDrawer( .clk(clk_25), .reset(rst), 
                                .h_cnt(h_cnt), .v_cnt(v_cnt), .pixel(pixel) );
    
endmodule
