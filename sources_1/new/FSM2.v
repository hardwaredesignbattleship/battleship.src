`include "../TransmissionConstants.vh"
`include "DisplayConstants.vh"
`include "StorageConstants.vh"
`include "KeyboardConstants.vh"

`define STATE_INIT  1
`define STATE_PLACE 2
`define STATE_ATK   3
`define STATE_WAIT  4
 
 `define SHIP_1X1_BASE  {99'b0,1'b1}
`define SHIP_1X3_BASE  {97'b0,3'b111}
`define SHIP_1X5_BASE  {95'b0,5'b1_1111}
`define SHIP_3X1_BASE  {79'b0,1'b1,9'b0,1'b1,9'b0,1'b1}
`define SHIP_5X1_BASE  {59'b0,1'b1,9'b0,1'b1,9'b0,1'b1,9'b0,1'b1,9'b0,1'b1}

`define  WPN_1X1_BASE {99'b0,1'b1}
`define  WPN_2X2_BASE {88'b0,2'b11,8'b0,2'b11}

module FSM2(
    input clk,
    input lose, 
    input button,
    input rx_has_data, //Not one pulse
    input [7:0] rx_data,
    output reg tx_has_data,
    input tx_available,
    output reg [7:0] tx_data,
    
    //Keyboard
    input [511:0] key_down,
    input [8:0] key_last_change,
    input key_valid,
    
    input [99:0] storage_read_ship,
    input [99:0] storage_read_hit,
    input [99:0] storage_read_destroy,
    input [99:0] storage_read_cusor,
    
    input [99:0] storage_read_op_hit,
    input [99:0] storage_read_op_destroy,
    
    output [2:0] storage_write_select,
    output [99:0] storage_write_value,
    output storage_write_enable,
    
    output reg [2:0] screen_select,
    
    output reg [5:0] lights,
    input weapon_type, //0->1x1, 1->2x2
    //input [2:0] ship_select,
    input ship_rotate,
    
    output [3:0] debug_rtshift,
    output [3:0] debug_dnshift
    );
    
    reg[2:0] state = `STATE_INIT;
    
    //wire pulse_rx_has_data;
    //OnePulse pulseRx( .clock(clk), .signal(rx_has_data), .signal_single_pulse(pulse_rx_has_data) );
    
    reg [5:0] rx_index;
    reg rx_hit_result;
    
    reg [99:0] storage_write_fsm = 0;
    reg [99:0] storage_write_uart = 0;
    reg [2:0] storage_write_sel_fsm = 0;
    reg [2:0] storage_write_sel_uart = 0;
    reg storage_write_mux = 0; //0->fsm, 1->uart
    
    assign storage_write_select = (storage_write_mux == 0)? storage_write_sel_fsm : storage_write_sel_uart;
    assign storage_write_value = (storage_write_mux == 0)? storage_write_fsm : storage_write_uart;
    //assign storage_write_enable = (storage_write_mux == 0)? storage_write_en_fsm : storage_write_en_uart;
    assign storage_write_enable = ~clk;
    
    wire [2:0] weapon_block_number;
    assign weapon_block_number = (weapon_type == 0)? 1 : 4;
    reg [2:0] attack_finish_counter = 0;
    
    wire [7:0] send_data [0:3];
    assign send_data[0] = {dnshift*10+rtshift,1'b0};
    assign send_data[1] = {dnshift*10+rtshift+1,1'b0};
    assign send_data[2] = {(dnshift+1)*10+rtshift,1'b0};
    assign send_data[3] = {(dnshift+1)*10+rtshift+1,1'b0};
    
    always @(*) begin
    	if(state == `STATE_PLACE) begin
    		case(ship_num)
    			6:	lights = 6'b11_1111;
    			5: lights = 6'b01_1111;
    			4: lights = 6'b00_1111;
    			3: lights = 6'b00_0111;
    			2: lights = 6'b00_0011;
    			1: lights = 6'b00_0001;
    			default: lights = 6'b00_0000;
    		endcase
    	end
    	else
    		lights = 6'b00_0000;
    end		
  
    always @(posedge clk) begin
    	if(lose) begin
    		state <= `STATE_INIT;
        end
        else begin
        case(state)
            `STATE_INIT: begin
                screen_select <= `SCENE_START;
                if(rx_has_data == 1) begin
                    if(rx_data == `TRANS_PLACE) begin
                        state <= `STATE_PLACE;
                    end
                end
            end
            
            `STATE_PLACE: begin
            	storage_write_mux <= 0;
                screen_select <= `SCENE_PLACE;
                if(button) begin
                	tx_has_data <= 1;
                	tx_data <= `TRANS_FINISH_PLACE;
                end
                else begin
                	tx_has_data <= 0;
                end
                if(rx_has_data == 1) begin
                    case(rx_data)
                        `TRANS_ROLE_ATK: begin
                            state <= `STATE_ATK;
                        end
                        
                        `TRANS_ROLE_WAIT: begin
                            state <= `STATE_WAIT;
                        end
                        
                        default: state <= state;
                    endcase
                end
            end
            
            `STATE_ATK: begin
                /*
                if(pulse_rx_has_data == 1) begin
                    rx_index <= rx_data[7:1];
                    rx_hit_result <= rx_data[0];   
                end
                */
                storage_write_mux <= 1;
                screen_select <= `SCENE_ENEMY;
                
                if(button) begin
                	tx_has_data <= 1;
                	tx_data <= {dnshift*10+rtshift,1'b0};
                end
                if(rx_has_data == 1) begin
                    if(attack_finish_counter < weapon_block_number) begin
                        attack_finish_counter <= attack_finish_counter + 1;
                        if(attack_finish_counter + 1< weapon_block_number) begin
                        	tx_has_data <= 1;
                        	tx_data <= send_data[attack_finish_counter];
                        end
                    end
                    else begin
                        
                        tx_has_data <= 1;
                        tx_data <= `TRANS_FINISH_ATTACK;
                        
                        attack_finish_counter <= 0;
                        
                        //Switch to wait automatically
                        state <= `STATE_WAIT;
                    end
                end
                else begin
                	tx_has_data <= 0;
                end
            end
            
            `STATE_WAIT: begin
            	storage_write_mux <= 1;
            	screen_select <= `SCENE_SELF;
                if(rx_has_data) begin
                    if(rx_data == `TRANS_SWITCH_ROLE) begin
                        state <= `STATE_ATK;
                        tx_has_data <= 0;
                    end
                    else begin
                        if(rx_data[7:1] >= 0 && rx_data[7:1] < 100) begin
                            tx_has_data <= 1;
                            tx_data[7:1] <= rx_data[7:1];
                            
                            if(storage_read_ship[tx_data[7:1]] == 1) tx_data[0] <= 1;
                            else tx_data[0] <= 0;
                        end
                        else begin
                            tx_has_data <= 0;
                        end
                    end
                end
                else   tx_has_data <= 0;
            end
            
        endcase
        end
    end
    
    always @(*) begin
        if(state == `STATE_ATK) begin
            if(rx_has_data == 1) begin
                rx_index = rx_data[7:1];
                rx_hit_result = rx_data[0];
                
                if(rx_index >= 0 && rx_index < 100) begin
                    storage_write_uart = storage_read_op_hit;
                    
                    storage_write_sel_uart = `FIELD_TYPE_OP_HIT;
                    storage_write_uart[rx_index] = 1;
                    //storage_write_en_uart = 0;
                    
                    if(rx_hit_result == 1) begin
                        storage_write_sel_uart = `FIELD_TYPE_OP_DESTROY;
                        storage_write_uart = storage_read_op_destroy;
                        
                        storage_write_uart[rx_index] = 1;
                        //storage_write_en_uart = 0;
                    end
                end
            end
        end
        else if(state == `STATE_WAIT) begin
        	if(rx_has_data == 1) begin
        		 rx_index = rx_data[7:1];
        	     rx_hit_result = rx_data[0];
        	     
        	     if(rx_index >= 0 && rx_index < 100) begin
        	     	storage_write_uart = storage_read_hit;
        	     	
        	     	storage_write_sel_uart = `FIELD_TYPE_HIT;
        	     	//storage_write_en_uart = 1;
        	     	storage_write_uart[rx_index] = 1;
        	     	if(storage_read_ship[rx_data[7:1]] == 1) begin
        	     		storage_write_sel_uart = `FIELD_TYPE_DESTROY;
        	     		storage_write_uart = storage_read_destroy;
        	                          
        	     		//storage_write_en_uart = 1;
        	     		storage_write_uart[rx_index] = 1;   		
        	     	end
        	     end
        	end
        end
    end
    
    reg [2:0] ship_num = 6;
    reg [3:0] rtshift = 0;
    //reg [3:0] next_rtshift = 0;		//right shift "on map"
    reg [3:0] dnshift = 0;
    //reg [3:0] next_dnshift = 0;	//down shift "on map"
    
    assign debug_dnshift = dnshift;
    assign debug_rtshift = rtshift;
    
    reg [3:0] add_vertical_len, add_horizontal_len;
    always @(*) begin
    	if(state == `STATE_PLACE) begin
    		if(ship_num >= 5) begin
    			if(ship_rotate) begin
    				add_vertical_len = 4;
    				add_horizontal_len = 0;
    			end
    			else begin
    				add_vertical_len = 0;
    		    	add_horizontal_len = 4;
    			end
    		end
    		else if(ship_num >= 3) begin
    			if(ship_rotate) begin
    				add_vertical_len = 2;
    		    	add_horizontal_len = 0;
    			end
    			else begin
    				add_vertical_len = 0;
    		    	add_horizontal_len = 2;
    			end
    		end
    		else if(ship_num >= 1) begin
    			add_vertical_len = 0;
    			add_horizontal_len = 0;
    		end
    	end
    	else if(state == `STATE_ATK) begin
    		if(weapon_type == 0) begin
    			add_vertical_len = 0;
    		    add_horizontal_len = 0;
    		end
    		else begin
    			add_vertical_len = 1;
    		    add_horizontal_len = 1;
    		end
    	end
    	else begin
    		add_vertical_len = 0;
    	    add_horizontal_len = 0;
    	end
    end
    
    wire pulse_key_valid;
    OnePulse pulseKeyValid( .clock(clk), .signal(key_valid), .signal_single_pulse(pulse_key_valid) );
    
    reg [99:0] ship_base;
    always @(*) begin
        case(ship_num)
            6: begin
                if(ship_rotate) ship_base = `SHIP_5X1_BASE;
                else ship_base = `SHIP_1X5_BASE;
            end
            
            5: begin
                if(ship_rotate) ship_base = `SHIP_5X1_BASE;
                else ship_base = `SHIP_1X5_BASE;
            end
            
            4: begin
                if(ship_rotate) ship_base = `SHIP_3X1_BASE;
                else ship_base = `SHIP_1X3_BASE;
            end
            
            3: begin
                if(ship_rotate) ship_base = `SHIP_3X1_BASE;
                else ship_base = `SHIP_1X3_BASE;
            end
            
            2: begin
                if(ship_rotate) ship_base = `SHIP_1X1_BASE;
                else ship_base = `SHIP_1X1_BASE;
            end
            
            1: begin
                if(ship_rotate) ship_base = `SHIP_1X1_BASE;
                else ship_base = `SHIP_1X1_BASE;
            end
            
            default: ship_base = `SHIP_1X1_BASE;
        endcase
    end
    
    wire[99:0] storage_ship_cusor_comb = (storage_read_ship | storage_read_cusor);
    
    always @(*) begin
        if(state == `STATE_PLACE && 
           pulse_key_valid == 1) begin
           
            if(key_down[`ENTER] == 1'b1 && |(storage_read_ship & storage_read_cusor) == 0 && ship_num > 0) begin
                storage_write_sel_fsm = `FIELD_TYPE_SHIP;
            end
            else begin
                storage_write_sel_fsm = `FIELD_TYPE_CUSOR;
            end
        end
    end
    
    always @(posedge clk) begin
        if(pulse_key_valid == 1 && key_down[key_last_change] == 1'b1) begin
            case(state)
            	`STATE_INIT: begin
            		ship_num <= 6;
            		rtshift <= 0;
            		dnshift <= 0;
            	end
                `STATE_PLACE: begin
  					
                	if(key_last_change == `UP && dnshift > 0) begin
                		//storage_write_sel_fsm <= `FIELD_TYPE_CUSOR;
                		dnshift <= dnshift - 1;
                		storage_write_fsm <= ship_base << ( (dnshift - 1) * 10 + rtshift);
                    end
          			else if(key_last_change == `LEFT && rtshift > 0) begin
          				//storage_write_sel_fsm <= `FIELD_TYPE_CUSOR;
          				rtshift <= rtshift - 1;
          				storage_write_fsm <= ship_base << ( dnshift * 10 + (rtshift - 1));
          	        end
          			else if(key_last_change == `RIGHT && rtshift + add_horizontal_len  < 9) begin
          				//storage_write_sel_fsm <= `FIELD_TYPE_CUSOR;
          				rtshift <= rtshift + 1;
          				storage_write_fsm <= ship_base << ( dnshift * 10 + (rtshift + 1));
          			end
          			else if(key_last_change == `DOWN && dnshift + add_vertical_len < 9) begin
          			    //storage_write_sel_fsm <= `FIELD_TYPE_CUSOR;
          				dnshift <= dnshift + 1;
          				storage_write_fsm <= ship_base << ( (dnshift + 1) * 10 + rtshift);
          			end
          			else if(key_last_change == `ENTER && ship_num > 0) begin
          				//storage_write_sel_fsm <= `FIELD_TYPE_SHIP;
          				storage_write_fsm <= storage_ship_cusor_comb;
          				ship_num <= ship_num - 1;
          			end
          			else begin
          			   //storage_write_sel_fsm <= storage_write_sel_fsm;
          			   storage_write_fsm <= storage_write_fsm;
          			end
                end
                
                `STATE_ATK: begin
                    if(key_last_change == `UP && dnshift > 0)
                     	dnshift <= dnshift - 1;
                     else if(key_last_change == `LEFT && rtshift > 0)
                     	rtshift <= rtshift - 1;
                    else if(key_last_change == `RIGHT && rtshift + add_horizontal_len  < 9)
                    	rtshift <= rtshift + 1;
                    else if(key_last_change == `DOWN && dnshift + add_vertical_len < 9)
                      	dnshift <= dnshift + 1;
                    case(weapon_type)
                    	0: begin
                    		storage_write_fsm <= `WPN_1X1_BASE << (dnshift*10+rtshift);
                    	end
                    	1: begin
                    		storage_write_fsm <= `WPN_2X2_BASE << (dnshift*10+rtshift);
                    	end
                    endcase
                end
            endcase
        end
    end
    
endmodule
