`timescale 100ns / 100ps

module Display_t;
    
    reg clk = 0;
    initial begin
        forever begin
            #1 clk = ~clk;
        end
    end
    
    wire clk_25;
    clk_wiz_0 clockMod( .clk(clk), .clk_out25(clk_25) );
    
    reg[99:0] storageShip = 2;
    reg[99:0] storageDestroy = 0;
    reg[99:0] storageCusor = 0;
    
    reg[9:0] h_cnt = 0;
    reg[9:0] v_cnt = 0;
    
    reg rst = 0;
    wire[11:0] pixel;
    Display display( .clk_25(clk_25), .reset(rst),
                     .h_cnt(h_cnt), .v_cnt(v_cnt),
                     .pixel(pixel),
                     .storage_ship(storageShip),
                     .storage_destroy(storageDestroy),
                     .storage_cusor(storageCusor) );
                     
    reg[1:0] roundCounter = 0;
    always @(posedge clk_25) begin
        if(h_cnt < 320) h_cnt <= h_cnt + 1;
        else begin
            h_cnt <= 0;
            if(v_cnt < 240) v_cnt <= v_cnt + 1;
            else begin
                v_cnt <= 0;
                if(roundCounter < 2) roundCounter <= roundCounter + 1;
                else $finish;
            end
        end
    end
    
endmodule
