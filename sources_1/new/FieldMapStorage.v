`timescale 1ns / 1ps

`include "StorageConstants.vh"

module FieldMapStorage(
    input clk,
    input reset,
    input [2:0] category,
    input write_enable,
    input [99:0] write_value,
    
    output [99:0] read_value_ship,
    output [99:0] read_value_destroy,
    output [99:0] read_value_hit,
    output [99:0] read_value_cusor,
    
    //Enemy's
     output [99:0] read_value_op_destroy,
     output [99:0] read_value_op_hit
    );
    
    reg[99:0] storageShip = 0;
    reg[99:0] storageDestroy = 0;
    reg[99:0] storageCusor = 0;
    reg[99:0] storageHit = 0;
    
    reg[99:0] storageOpDestroy = 0;
    reg[99:0] storageOpHit = 0;
    
    assign read_value_ship = storageShip;
    assign read_value_destroy = storageDestroy;
    assign read_value_cusor = storageCusor;
    assign read_value_hit = storageHit;
    
    assign read_value_op_destroy = storageOpDestroy;
    assign read_value_op_hit = storageOpHit;
    
    always @(posedge clk or posedge write_enable) begin
        /*
        if(reset == 1) begin
            storageShip <= 0;
            storageDestroy <= 0;
            storageCusor <= 0;
            storageHit <= 0;
            
            storageOpDestroy <= 0;
            storageOpHit <= 0;
        end
        else */if(write_enable == 1) begin
            case(category)
                `FIELD_TYPE_SHIP: storageShip <= write_value;
                `FIELD_TYPE_DESTROY: storageDestroy <= write_value;
                `FIELD_TYPE_CUSOR: storageCusor <= write_value;
                `FIELD_TYPE_HIT: storageHit <= write_value;
                
                `FIELD_TYPE_OP_DESTROY: storageOpDestroy <= write_value;
                `FIELD_TYPE_OP_HIT: storageOpHit <= write_value;
            endcase
        end
        else begin
            storageShip <= storageShip;
            storageDestroy <= storageDestroy;
            storageCusor <= storageCusor;
            storageHit <= storageHit;
            
            storageOpDestroy <= storageOpDestroy;
            storageOpHit <= storageOpHit;
        end
    end
    
endmodule
