`timescale 1ns / 1ps

module TestUartTop(
    input clk,
    input reset,
    input add_but,
    output uart_tx,
    input uart_rx,
    output [3:0] DIGIT,
    output [6:0] DISPLAY
    );
    
    wire clkDisplay;
    CustomClock #(16) displayClock( .clk(clk), .clk_out(clkDisplay) );
    
    reg [7:0] value = 0;
    wire[3:0] bcd0;
    wire[3:0] bcd1;
    assign bcd0 = (value % 10);
    assign bcd1 = (value / 10);
    SevenSegDecoder ssDecoder( .clk(clkDisplay), .BCD0(bcd0), .BCD1(bcd1),
                                .DISPLAY(DISPLAY), .DIGIT(DIGIT));

    //wire clkDebounce;
    //CustomClock #(13) dbClock( .clk(clk), .out_clk(clkDebounce) );
    wire db_but_add;
    wire pulse_but_add;
    PushButDebouncer dbButAdd( .clk(clk), .pb(add_but), .pb_debounced(db_but_add) );
    OnePulse pulseButAdd( .clk(clk), .sigInput(db_but_add), .sigOutput(pulse_but_add) );
        
    wire has_rx_data;
    wire [7:0] rx_data;
    UartModule uart( .clk(clk), .reset(reset),
                     .uart_tx(uart_tx), .uart_rx(uart_rx),
                     .load_tx_data(pulse_but_add), .tx_out_data(value),
                     .has_rx_data(has_rx_data), .rx_in_data(rx_data) );
                     
    always @(posedge clk) begin
        if(has_rx_data == 1) begin
            value <= rx_data;
        end
        else begin
            value <= value;
        end
    end
    
endmodule
