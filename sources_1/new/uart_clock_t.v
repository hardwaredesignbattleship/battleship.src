`timescale 10ns / 100ps

module uart_clock_t;
    
    reg clk = 0;
    initial begin
        forever begin
            #1 clk = ~clk;
        end
    end
    
    wire uart_clk;
    uart_clock uartClk( .clk(clk), .out_clk(uart_clk) );
    
    initial begin
        #1000000
        $finish;
    end
endmodule
