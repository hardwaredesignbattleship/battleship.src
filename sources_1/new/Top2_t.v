`timescale 100ns / 1ps

module Top2_t;

    reg clk = 0;
    initial begin
        forever begin
            #1 clk = ~clk;
        end
    end
    
    Top2 topMod( .clk(clk) );
    
    initial begin
        #10000000
        $finish;
    end
endmodule
