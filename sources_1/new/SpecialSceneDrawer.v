`timescale 1ns / 1ps

module SpecialSceneDrawer(
    input clk_25,
    input reset,
    input [2:0] scene_type,
    input [9:0] h_cnt,
    input [9:0] v_cnt,
    output [11:0] pixel
    );
    
    assign pixel = 12'b1111_1111_1111; //Reserved
    
endmodule
