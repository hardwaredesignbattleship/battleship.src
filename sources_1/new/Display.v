`timescale 1ns / 1ps

`include "DisplayConstants.vh"

module Display(
    input clk_25,
    input reset,
    input [9:0] h_cnt,
    input [9:0] v_cnt,
    output reg [11:0] pixel,
    
    input [99:0] storage_ship,
    input [99:0] storage_destroy,
    input [99:0] storage_hit,
    input [99:0] storage_cusor,
    
    input [2:0] scene_select,
    input [99:0] storage_enemy_destroy,
    input [99:0] storage_enemy_hit
    );
    
    wire[11:0] block_pixel;
    MapBlockDrawer blockDrawer( .clk(clk_25), .reset(reset), .h_cnt(h_cnt), .v_cnt(v_cnt), 
                                .pixel(block_pixel));
                                
    wire [11:0] special_pixel;
    SpecialSceneDrawer specialDrawer( .clk_25(clk_25), .reset(), 
                                      .scene_type(scene_select),
                                      .v_cnt(v_cnt),
                                      .h_cnt(h_cnt),
                                      .pixel(special_pixel) );
                                
    wire[9:0] actual_v_cnt;
    wire[9:0] actual_h_cnt;
    assign actual_v_cnt[9:0] = (v_cnt >> `REAL_SCREEN_FACTOR);
    assign actual_h_cnt[9:0] = (h_cnt >> `REAL_SCREEN_FACTOR);
    
    wire[3:0] horIndex;
    wire[3:0] verIndex;
    assign horIndex = (actual_h_cnt - `PADDING) / `MAP_BLOCK_LENGTH;
    assign verIndex = actual_v_cnt / `MAP_BLOCK_LENGTH;
    wire[6:0] storageAddr;
    assign storageAddr = (verIndex * 10 + horIndex);
    
    reg pixelShip, pixelDestroy, pixelHit, pixelCusor;
    always @(*) begin
        case(scene_select)
            `SCENE_SELF: begin
                pixelShip = storage_ship[storageAddr];
                pixelDestroy = storage_destroy[storageAddr];
                pixelHit = storage_hit[storageAddr];
                pixelCusor = storage_cusor[storageAddr];
            end
            
            `SCENE_PLACE: begin
                pixelShip = storage_ship[storageAddr];
                pixelDestroy = storage_destroy[storageAddr];
                pixelHit = storage_hit[storageAddr];
                pixelCusor = storage_cusor[storageAddr];
            end
            
            `SCENE_ENEMY: begin
                pixelShip = 0;
                pixelDestroy = storage_enemy_destroy[storageAddr];
                pixelHit = storage_enemy_hit[storageAddr];
                pixelCusor = 0;
            end
            
            default: begin
                pixelShip = 0;
                pixelDestroy = 0;
                pixelHit = 0;
                pixelCusor = 0;
            end
        endcase
    end
    
    always @(*) begin
        if(scene_select == `SCENE_SELF || 
           scene_select == `SCENE_ENEMY || 
           scene_select == `SCENE_PLACE) begin
            if(actual_h_cnt >= `SCREEN_HOR_LOWER_BOUND && 
               actual_h_cnt <= `SCREEN_HOR_UPPER_BOUND) begin
                
                if(pixelCusor == 1) pixel = `COLOR_YELLOW;
                else if(pixelDestroy == 1) pixel = `COLOR_RED;
                else if(pixelHit == 1) pixel = `COLOR_PURPLE;
                else if(pixelShip == 1) pixel = `COLOR_BLUE;
                else pixel = 0; 
                
                pixel = pixel | block_pixel;
            end
            else pixel = block_pixel;
        end
        else if(scene_select == `SCENE_START || scene_select == `SCENE_END) begin
            pixel = special_pixel;
        end
        else pixel = 0;
    end
    
endmodule
