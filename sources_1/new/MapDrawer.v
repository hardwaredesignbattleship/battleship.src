`timescale 1ns / 1ps

`include "DisplayConstants.vh"

module MapBlockDrawer(
    input clk,
    input reset,
    input [9:0] h_cnt,
    input [9:0] v_cnt,
    output reg [11:0] pixel
    );
    
    wire[9:0] actual_v_cnt;
    wire[9:0] actual_h_cnt;
    assign actual_v_cnt[9:0] = (v_cnt >> `REAL_SCREEN_FACTOR);
    assign actual_h_cnt[9:0] = (h_cnt >> `REAL_SCREEN_FACTOR);
    
    always @(*) begin
        if(actual_h_cnt < `SCREEN_HOR_LOWER_BOUND || 
           actual_h_cnt > `SCREEN_HOR_UPPER_BOUND) pixel = 12'b0;
        else begin
            if( ((actual_h_cnt - `PADDING) % `MAP_BLOCK_LENGTH == 0) ||
                (actual_v_cnt % `MAP_BLOCK_LENGTH == 0) ) pixel = 12'b1111_1111_1111;
            else pixel = 12'b0;
        end
    end

endmodule
