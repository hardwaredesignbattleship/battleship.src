`timescale 1ns / 1ps

`include "StorageConstants.vh"

module TestScreenTop2(
    input clk,
    input rst,
    output [3:0] vgaRed,
    output [3:0] vgaGreen,
    output [3:0] vgaBlue,
    output hsync,
    output vsync
    );
    
    wire clk_25;
    clk_wiz_0 clockMod( .clk(clk), .clk_out25(clk_25) );
    
    wire valid;
    wire [9:0] h_cnt; //640
    wire [9:0] v_cnt;  //480
    wire [11:0] pixel;

    assign {vgaRed, vgaGreen, vgaBlue} = (valid==1'b1) ? pixel : 12'h0;
    
    vga_controller   vga_inst(
      .pclk(clk_25),
      .reset(rst),
      .hsync(hsync),
      .vsync(vsync),
      .valid(valid),
      .h_cnt(h_cnt),
      .v_cnt(v_cnt)
    );
    
    reg[2:0] category = `FIELD_TYPE_DESTROY;
    reg write_enable = 0;
    reg[99:0] write_value = 0;
    wire[99:0] read_value_ship;
    wire[99:0] read_value_destroy;
    wire[99:0] read_value_cusor;
    FieldMapStorage storage( .reset(rst), .category(category), 
                             .write_enable(write_enable), .write_value(write_value),
                             .read_value_ship(read_value_ship), 
                             .read_value_destroy(read_value_destroy), 
                             .read_value_cusor(read_value_cusor) );
                             
    Display displayMod( .clk_25(clk_25), .reset(rst), .h_cnt(h_cnt), .v_cnt(v_cnt),
                        .storage_ship(read_value_ship),
                        .storage_destroy(read_value_destroy),
                        .storage_cusor(read_value_cusor),
                        .pixel(pixel) );
                        
    reg[2:0] initCounter = 0;
    always @(posedge clk_25) begin
        if(initCounter < 4) begin
            initCounter <= initCounter + 1;
            case(initCounter)
                0: begin
                    write_enable <= 1;
                    write_value <= 14 << 11;
                end
                
                1: begin
                    write_enable <= 0;
                    category <= `FIELD_TYPE_SHIP;
                end
                
                2: begin
                    write_enable <= 1;
                    write_value <= 6 << 21;    
                end
                
                default: write_enable <= 0;
            endcase
        end
        else begin
            initCounter <= initCounter;
        end
    end
      
endmodule
