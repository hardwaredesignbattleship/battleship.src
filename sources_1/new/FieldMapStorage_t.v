`timescale 100ns / 1ps

`include "StorageConstants.vh"

module FieldMapStorage_t;

    reg reset = 0;
    reg[2:0] category = `FIELD_TYPE_SHIP;
    wire[99:0] read_value;
    reg[99:0] write_value = 0;
    reg write_enable = 0;
    FieldMapStorage storage( .reset(reset), .category(category),
                             .read_value(read_value), .write_value(write_value), .write_enable(write_enable) );
        
    wire[9:0] readValueMonitor;
    assign readValueMonitor = read_value[9:0];
                         
    initial begin
        #10
        write_value[1] = 1;
        write_enable = 1;
        #1
        write_enable = 0;
        category = `FIELD_TYPE_DESTROY;
        #5
        $finish;
    end
endmodule
