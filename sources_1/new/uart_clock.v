`timescale 1ns / 1ps

module uart_clock(
    input clk,
    output out_clk
    );
    
    parameter baud_rate = 115200;
    parameter sys_freq = 10**8;
    parameter factor = 0;
    
    integer cycle_half = (((sys_freq / baud_rate) >> factor) >> 1) - 1;
    
    integer counter = 0;
    reg r_out_clk = 0;
    assign out_clk = r_out_clk;
    always @(posedge clk) begin
        if(counter >= cycle_half) begin
            counter <= 0;
            r_out_clk <= ~r_out_clk;
        end
        else begin
            counter <= counter + 1;
            r_out_clk <= r_out_clk;
        end
    end
    
endmodule
